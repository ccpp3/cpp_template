cmake_minimum_required (VERSION 3.5)

# Add sub directories
add_subdirectory(src)
add_subdirectory(test)
