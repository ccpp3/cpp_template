#include <iostream>
#include <limits>

using namespace std;

#include "calculate.h"

int main() {
    int first_number, second_number;

    cout << "Enter two number: ";
    cin >> first_number >> second_number;

    Calculate cal;

    cout << "ADD      result: " << cal.Calculation(ADD, first_number, second_number) << endl;
    cout << "MINUS    result: " << cal.Calculation(MINUS, first_number, second_number) << endl;
    cout << "MULTIPLE result: " << cal.Calculation(MULTIPLE, first_number, second_number) << endl;
    cout << "DIVIDE   result: " << cal.Calculation(DIVIDE, first_number, second_number) << endl;

    return 0;
}
