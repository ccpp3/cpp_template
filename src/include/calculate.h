#ifndef CALCULATE_H
#define CALCULATE_H

enum CalcType {
    ADD,
    MINUS,
    MULTIPLE,
    DIVIDE
};

class Calculate {
public:
    int     Calculation(CalcType op, int a, int b);
    double  Calculation(CalcType op, double a, double b);
};

#endif /* CALCULATE_H */