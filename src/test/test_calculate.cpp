#include <iostream>
using namespace std;

#include "gtest/gtest.h"
#include "calculate.h"



class ClaculateTest : public ::testing::Test {
protected:

    void SetUp() override {
        cout << "SetUp runs before each case." << endl;
    }

    void TearDown() override {
        cout << "TearDown runs after each case." << endl;
    }

    Calculate calculate;
};

TEST(TestCalculationInt, CalculationInt) {
    Calculate calculate;
    EXPECT_EQ(calculate.Calculation(ADD, 1, 1), 2);
    EXPECT_EQ(calculate.Calculation(MINUS, 2, 1), 1);
    EXPECT_EQ(calculate.Calculation(MULTIPLE, 3, 3), 9);
    EXPECT_EQ(calculate.Calculation(DIVIDE, 10, 2), 5);
    EXPECT_GT(calculate.Calculation(DIVIDE, 10, 0), 999999999);
}

TEST_F(ClaculateTest, CalculationDouble) {
    EXPECT_EQ(calculate.Calculation(ADD, 1.1, 1.1), 2.2);
}
