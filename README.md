# C++ project template

[![pipeline status](https://gitlab.com/ccpp3/cpp_template/badges/master/pipeline.svg)](https://gitlab.com/ccpp3/cpp_template/-/commits/master)
[![coverage report](https://gitlab.com/ccpp3/cpp_template/badges/master/coverage.svg)](https://gitlab.com/ccpp3/cpp_template/-/commits/master)

## Description

This is a template of a C++ project


## Project used

- Make system
  - [cmake](https://cmake.org/)
  - [cmake-modules](https://github.com/bilke/cmake-modules)
- Unit testing
  - [googletest](https://github.com/google/googletest)
- Code lint
- Code coverage
  - [gcov](https://gcc.gnu.org/onlinedocs/gcc/Gcov.html)
  - [lcov](https://ltp.sourceforge.net/coverage/lcov.php)